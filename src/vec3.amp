mod pt::vec3;

use extern "C" "math.h";

define Vec3 = struct(x: float32, y: float32, z: float32);

fn __neg__: (a: Vec3) -> Vec3 = {
    return Vec3(x: -a.x, y: -a.y, z: -a.z);
}

fn __add__: (a: Vec3, b: Vec3) -> Vec3 = {
    return Vec3(x: a.x + b.x, y: a.y + b.y, z: a.z + b.z);
}

fn __sub__: (a: Vec3, b: Vec3) -> Vec3 = {
    return Vec3(x: a.x - b.x, y: a.y - b.y, z: a.z - b.z);
}

fn __mul__: (a: Vec3, b: Vec3) -> Vec3 = {
    return Vec3(x: a.x * b.x, y: a.y * b.y, z: a.z * b.z);
}

fn __div__: (a: Vec3, b: Vec3) -> Vec3 = {
    return Vec3(x: a.x / b.x, y: a.y / b.y, z: a.z / b.z);
}

fn __mul__: (a: Vec3, b: float32) -> Vec3 = {
    return Vec3(x: a.x * b, y: a.y * b, z: a.z * b);
}

fn __div__: (a: Vec3, b: float32) -> Vec3 = {
    return a * (1.0 / b);
}

fn dot: (a: Vec3, b: Vec3) -> float32 = {
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

fn len2: (a: Vec3) -> float32 = {
    return dot(a, a);
}

fn len: (a: Vec3) -> float32 = {
    return sqrtf(len2(a));
}

fn normalize: (a: Vec3) -> Vec3 = {
    return a / len(a);
}

fn cross: (a: Vec3, b: Vec3) -> Vec3 = {
    return Vec3(
        x: a.y * b.z - a.z * b.y,
        y: a.z * b.x - a.x * b.z,
        z: a.x * b.y - a.y * b.x,
   );
}
